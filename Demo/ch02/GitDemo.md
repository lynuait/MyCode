教程
https://github.com/FrankFrankWei/git-gui-tutorial-article/blob/master/git-gui-tutorial-done-remote.md

本文旨在分享通过鼠标点击GUI界面使用Git实现日常开发的版本管理。尽管仅使用少量的Git命令就能够满足日常版本管理，一上手就展示命令行还让是不少想了解Git的人望而却步。
看完这篇文章，你应该对Git的工作流程有基本的了解，并能够使用Git GUI进行日常开发的版本管理。